import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpUtilService } from 'src/app/shared/services/http-util.service';
import { Agenda } from './models/agenda.model';
import { Funcionario } from './models/funcionario.model';
import { Servico } from './models/servico.model';
import { FuncionariosService } from './servico/funcionarios.service';
import { ServicosService } from './servico/servicos.service';

@Component({
  selector: 'app-agenda-list',
  templateUrl: './agenda-list.component.html',
  styleUrls: ['./agenda-list.component.scss'],
})
export class AgendaListComponent implements OnInit {
  servicos: Servico[];
  agenda: Agenda;
  funcionarios: Funcionario[];
  funcionarioId: String;
  servicoId: String;
  selected_value: String;
  customYearValues = [2020];
  customDayShortNames = ['s\u00f8n', 'man', 'tir', 'ons', 'tor', 'fre', 'l\u00f8r'];
  customPickerOptions: any;
  form: FormGroup;

  constructor(private service: ServicosService,
    private fb: FormBuilder,
    private router: Router,
    private funcionario: FuncionariosService,
    private httpUtil: HttpUtilService) {
  }

  ngOnInit(): void {
    this.listarServico();
    this.gerarForm();
  }

  gerarForm() {
    this.form = this.fb.group({
      data: ['', []],
      servico: [this.servId, []],
      funcionario: ['', []]
    })
  }

  listarServico() {
    this.service.listarServico()
      .subscribe(
        data => {
          this.servicos = data
        },
        err => {
          let msg: string = "Não foi possivel obter serviços";
          alert(msg);
        }
      );
  }

   get servId(): string {
    return sessionStorage['servicoId'] || false;
  }

  obterFuncionario(event) {
    if (event.target.value){
      this.servicoId = event.target.value;
    } else if (this.servId){
      this.servicoId = this.servId;
    }else{
      return;
    }
    sessionStorage['servicoId'] = this.servicoId;
    this.funcionario.buscarFuncionarioPorServico(this.servicoId).subscribe(
      data=>{
        this.funcionarios = data;
      }
    )
    
  }

  cadastrarAgendamento() {
    this.gerarForm();
    console.log(this.form.value);
  }


}
