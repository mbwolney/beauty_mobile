import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Login } from '../model/login.model';
import { LoginService } from '../service/login.service';

@Component({
  selector: 'app-login-list',
  templateUrl: './login-list.component.html',
  styleUrls: ['./login-list.component.scss'],
})
export class LoginListComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.gerarForm();
  }

  gerarForm() {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      senha: ['', [Validators.required, Validators.minLength(6)]]
    })
  }

  refresh(ev) {
    setTimeout(() => {
      ev.detail.complete();
    }, 3000);
  }

  logar() {
    if (this.form.invalid) {
      const msg: string = "Dados inválidos";
      alert(msg);
      return;
    }
    const login: Login = this.form.value;
    this.loginService.logar(login)
      .subscribe(
        data => {
          localStorage['token'] = data['data']['token'];
          this.router.navigate(['/home'])
        },
        err => {
          let msg: string = "Tente novamente em instantes";
          if (err['status'] == 401) {
            msg = "Email ou Senha inválido(s)."
          }
          alert(msg);
        }
      )
  }

}
